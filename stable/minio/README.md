
As mentioned in official minio Helm chart repo : https://github.com/minio/charts

> *Recommended:* A new operator based Helm Chart replaces this project. This repository will be archived after April 2021 

To install minio the new way using operator, refer to the documentation : https://min.io/docs/minio/kubernetes/upstream/operations/install-deploy-manage/deploy-operator-helm.html#deploy-operator-with-helm

and use charts already created : 

* [👉 minio-operator](../minio-operator/)

* [👉 minio-tenant](../minio-tenant/)
