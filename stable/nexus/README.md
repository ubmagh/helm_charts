# Nexus Migration Steps 

 ## For Old Nexus
1 - Configure and Run the Backup Task for OrientDB ( Admin - Export databases for backup). It will generate .bak files.

 ## For New Nexus
2 - Deploy  Nexus on Kubernetes

3 - Clean up All .bak files in **$data-dir/restore-from-backup**  directory.

4 - Remove the following directories from **$data-dir/db** :
- component
- config
- security

5 - Go to the location where you stored the exported databases (**Step 1**)

6 - Copy the corresponding .bak files to **$data-dir/restore-from-backup** for restoration, you can use : 

````
$ rsync -arv [SOURCE] [DEST]
````

7 - Copy the **blobs** directory  to the New Nexus **$data-dir/blobs** directory.

8 - Copy the **keystores/node/** directory  to the New Nexus **$data-dir/keystores/node/** directory.

9 - Change **blobs & restore-from-backup**Owner to Nexus (**ID 200**), by running :

```
$ sudo sudo chown -R 200:200 [DIR_PATH]
```

9 - Restart Nexus

**NB :** **$data-dir** referred to **/nexus-data/nexus3**
- Reference : [Backup And Restore Documentation](https://help.sonatype.com/repomanager3/backup-and-restore) 