
# COMPANY-NAME Vault Chart!

> **note** : to mount existing data, have a look on the file `mount-existing-PV-PVC.yaml` ! you might wanna deploy a PVC before installing Vault 

This chart is based on the official chart from Github : https://github.com/hashicorp/vault-helm

Current Version is from tag v0.14.0.


## Notes 

Once the helm chart is deployed, the vault server need to be initialized. 

The initialization generates the credentials necessary to unseal all the Vault server.


## Unseal Vault 

At first install, we need to manually unseal vault by running : `vault operator init` to get the unseal keys.

More info : https://www.vaultproject.io/docs/platform/k8s/helm/run

The unseal keys are then saved on the devops-keepass.